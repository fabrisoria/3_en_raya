<?php
session_start();
?>

<html>
<body>
<center>

<?php 
 include "jugador.php";
 include "tablero.php";
 include "logica.php";


if($_REQUEST)
{
	$tablero = new Tablero;
	$logica = new Logica;

	if($_POST){
		//SE HA DADO INICIO AL JUEGO
		$jugador1 = new Jugador($_POST['n1'], $_POST['n2']);

		//Se inicializa el tablero
		$tablero->iniciar();	
		$turno = 1; // Comienza el jugador 1
	}else
	{
		//EL JUEGO YA HA INICIADO Y SE REALIZÓ UNA JUGADA
		$turno = $_GET['turno'];
		$tablero->cambiar($_GET['pos'], $turno);
		//SE VERIFICA SI HAY UN GANADOR
		$logica->verificar();
	}

	echo "<h2>".$_SESSION["jugador1"]." vs. ".$_SESSION["jugador2"]."</h2>";
	//MOSTRAR EL TABLERO DE JUEGO
	if($turno==2){
			echo "El turno es para: ".$_SESSION["jugador2"]."<br><br>";
			$turno = 1;
		}
		else 
		{
			echo "El turno es para: ".$_SESSION["jugador1"]."<br><br>";
			$turno = 2;
		}
	echo '
		<table border="1">
		<tr>
			<td>'.$tablero->mostrar(11, $turno).'</td>
			<td>'.$tablero->mostrar(12, $turno).'</td>
			<td>'.$tablero->mostrar(13, $turno).'</td>
		</tr>
		<tr>
			<td>'.$tablero->mostrar(21, $turno).'</td>
			<td>'.$tablero->mostrar(22, $turno).'</td>
			<td>'.$tablero->mostrar(23, $turno).'</td>
		</tr>
		<tr>
			<td>'.$tablero->mostrar(31, $turno).'</td>
			<td>'.$tablero->mostrar(32, $turno).'</td>
			<td>'.$tablero->mostrar(33, $turno).'</td>
		</tr>
	</table>';

	//Boton para terminar el juego y volver a comenzar
	echo '<br>
	<input type="submit" name="terminar" value="Terminar Juego" onClick="location.href=\'index.php\'">
	<form name="empezar" action="index.php" method="post">
	<input type="hidden" name="n1" value="'.$_SESSION["jugador1"].'">
	<input type="hidden" name="n2" value="'.$_SESSION["jugador2"].'">
	<input type="submit" name="empezar" value="Volver a empezar"></form>';

}
else
{
	//EL JUEGO AÚN NO HA INICIADO
	//MOSTRAMOS EL FORMULARIO PARA INICIAR
	$jugador1 = new Jugador("Jugador 1", "Jugador 2");
	echo "<h2>Bienvenido al juego 3 en raya</h2>
	Que disfruteís del juego<br><br>";
	echo '
	<form name="juego" action="index.php" method="post">
	<table border="1">
	<tr>
		<td>'.$_SESSION["jugador1"].' juega con <img src="imagenes/x.gif" width="30" height="30"></td>
		<td>'.$_SESSION["jugador2"].' juega con <img src="imagenes/o.gif" width="50" height="50"></td>
	</tr>
	<tr>
		<td>Ingrese el nombre:<input type="text" name="n1" size="7"></td>
		<td>Ingrese el nombre:<input type="text" name="n2" size="7"></td>
	</tr>
	</table>
	<br>
	<input type="submit" value="COMENZAR JUEGO">
	</form>';
}

    
?>

<br><br>

</center>
</body>
</html>

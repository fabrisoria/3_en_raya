<?php
class Logica{
    
    
     private $tabla = [];
    private $next_player = "x"; // primero juega el jugador 1

    function __construct($tabla = []) {
        if ($tabla) {
            $this->tabla = $tabla;
        } else {
            $this->tabla = $this->initTable();
        }
    }

    function getTable() {
        return $this->tabla;
    }


    function play($cell) {

        switch($cell) {
            case 1: $i = 0; $j = 0; break;
            case 2: $i = 0; $j = 1; break;
            case 3: $i = 0; $j = 2; break;
            case 4: $i = 1; $j = 0; break;
            case 5: $i = 1; $j = 1; break;
            case 6: $i = 1; $j = 2; break;
            case 7: $i = 2; $j = 0; break;
            case 8: $i = 2; $j = 1; break;
            case 9: $i = 2; $j = 2; break;
        }


        if ($this->tabla[$i][$j] === _) {
            $this->tabla[$i][$j] = $this->next_player;

            // set the next player
            $this->next_player = $this->next_player === x ? o : x;

            return true;
        } else {
            return false;
        }
    }
    
	public function verificar(){
        $winner = $this->getWinner();
        if ($winner !== "") {
            return true;
        }

        for ($i=0; $i<3; $i++) {
            for ($j=0; $j<3; $j++) {
                if ($this->tabla[$i][$j] === "") {
                    return false;
                }
            }
        }
        return true;
 }
    
    function getWinner() {

        if ($winner = $this->checkHorizontal()) {
            return $winner;
        }

        if ($winner = $this->checkVertical()) {
            return $winner;
        }

        if ($winner = $this->checkDiagonal()) {
            return $winner;
        }
    }

    /**
     * @return winner
     */
    private function checkHorizontal() {
        for ($i=0; $i<3; $i++) {
            $winner = $this->tabla[$i][0];

            for ($j=0; $j<3; $j++) {
                if ($this->tabla[$i][$j] != $winner) {
                    $winner = null;
                    break;
                }
            }
            // if the winner is not null stop
            if ($winner !== null) {
                break;
            }
        }
        return $winner;
    }

    private function checkVertical() {
        for ($i=0; $i<3; $i++) {
            $winner = $this->tabla[0][$i];

            for ($j=0; $j<3; $j++) {
                if ($this->tabla[$j][$i] != $winner) {
                    $winner = null;
                    break;
                }
            }
            // if the winner is not null stop
            if ($winner !== null) {
                break;
            }
        }
        return $winner;
    }

    private function checkDiagonal() {
      
        // check the first diagonal
        $winner = $this->tabla[0][0];
        for ($i=0; $i<3; $i++) {
            if ($this->tabla[$i][$i] != $winner) {
                $winner = null;
                break;
            }
        }

        // if there's no winner check the second diagonal
        if ($winner === null) {
            $winner = $this->tabla[0][2];
            for ($i=0; $i<3; $i++) {
                if ($this->tabla[$i][2-$i] != $winner) {
                    $winner = null;
                    break;
                }
            }
        }

        return $winner;
    }
    
    private function initTable() {
        $table = [];
        for ($i=0; $i<3; $i++) {
            for ($j=0; $j<3; $j++) {
                $table[$i][$j] = "";
            }
        }
        return $table;
    }
    
}
?>